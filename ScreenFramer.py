"""
ScreenFramer
https://gitlab.com/Hyperion777/ScreenFramer
"""

from PIL import Image
import sys
import os
import json

# constants
SCRIPT_PATH=os.path.dirname(os.path.realpath(__file__))+'\\'
FRAMES_PATH=SCRIPT_PATH+'frames\\'
FRAMES_JSON=FRAMES_PATH+'frames.json'

# open the json file which defines the frames
with open(FRAMES_JSON) as json_data:
    frames = json.load(json_data)

# ask for the frame to use
print("Available frames:")
for index,frame in enumerate(frames):
	print(index,">",frame['name'])
while True:
	number = input("Select frame number: ")
	if number.isnumeric() and int(number)<len(frames):
		break
frame = frames[int(number)]

# cycle all parameters
for x in range(1, len(sys.argv)):
	# levels
	levelFrame = Image.open(FRAMES_PATH+frame['frameFile']).convert("RGBA")
	levelBackground = Image.new("RGBA", levelFrame.size)
	levelScreenshot = Image.open(sys.argv[x]).convert("RGBA").resize((frame['screenshotResizeTo'][0], frame['screenshotResizeTo'][1]),Image.BICUBIC)
	if 'screenGlare' in frame:
		levelGlare = Image.open(FRAMES_PATH+frame['screenGlare']).convert("RGBA")

	# apply levels
	levelBackground.paste(levelFrame, (0, 0), levelFrame)
	levelBackground.paste(levelScreenshot, (frame['screenshotPutAtXY'][0], frame['screenshotPutAtXY'][1]), levelScreenshot)
	if 'screenGlare' in frame:
		levelBackground = Image.alpha_composite(levelBackground,levelGlare)
	levelBackground.save(os.path.splitext(sys.argv[x])[0]+"_framed.png");
