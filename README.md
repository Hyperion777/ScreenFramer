# ScreenFramer

This is a tool to frame your screenshot into a real device frame.

## Installation
- Install [Python 3](https://www.python.org/)

- Install [Pillow for Python](https://pypi.python.org/pypi/Pillow/4.0.0)

## Usage
- Drag one or more screenshot over the script
- Select the frame you want to apply
- Enjoy your framed screenshot!

## Customization
If you want to add your own frame, you need to declare a new frame into /frames/frames.json.
As you can see from the samples, you need to define:
- name: the name of the device
- screenshotResizeTo: the screenshot will be resized to this size
- screenshotPutAtXY: the screenshot will be put in this XY coordinate of the frame frameFile
- frameFile: the image of the frame. the screenshot will be put over that
- screenGlare: (optional) the image of the glare effect of the glass. this image will be put over the screenshot
